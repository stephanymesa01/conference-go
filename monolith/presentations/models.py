from django.db import models
from django.urls import reverse


class Status(models.Model):
    """
    The Status model provides a status to a Presentation, which
    can be SUBMITTED, APPROVED, or REJECTED.

    Status is a Value Object and, therefore, does not have a
    direct URL to view it.
    """

    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("id",)  # Default ordering for Status
        verbose_name_plural = "statuses"  # Fix the pluralization


class Presentation(models.Model):
    """
    The Presentation model represents a presentation that a person
    wants to give at the conference.
    """

    presenter_name = models.CharField(max_length=150)
    company_name = models.CharField(max_length=150, null=True, blank=True)
    presenter_email = models.EmailField()

    title = models.CharField(max_length=200)
    synopsis = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    status = models.ForeignKey(
        Status,
        related_name="presentations",
        on_delete=models.PROTECT,
    )

    conference = models.ForeignKey(
        "events.Conference",
        related_name="presentations",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_show_presentation", kwargs={"id": self.id})

    def __str__(self):
        return self.title

    def approve(self):
        # defining a function
        # naming the method since it's in a class
        status = Status.objects.get(name="APPROVED")
        # this is an assignment = we are accessing the Status model
        # since it's a django model we can use the django model manager
        # with using objects. we use the get method
        # it will look for () inside the get method
        # we pass in a key word argument = in this case we have it look for
        # the column "name" and I want it to look for a value of Approved
        self.status = status
        # self is a instance of a class, we have a property called status
        # in this line we want the instance of status to be assigned to the
        # status property
        self.save()
        # we want to use the save method to save the instance

    def reject(self):
        status = Status.objects.get(name="REJECTED")
        self.status = status
        self.save()

    class Meta:
        ordering = ("title",)  # Default ordering for presentation

    @classmethod
    def create(cls, **kwargs):
        kwargs["status"] = Status.objects.get(name="SUBMITTED")
        presentation = cls(**kwargs)
        presentation.save()
        return presentation
